/**
 * funcion Lapiz, cuaderno, goma, regla, sacapuntas, boligrafo. 
 * Generan una codigo dinamico dentro de la pagina apartir de .innerHTML para modificar el contenido dentro del id  
 * Se crea una lista con los detalles de cada uno de los productos. 
 */
function lapiz() {
    document.getElementById("Lapiz").innerHTML = 
      "<ul>" +
        "<li>Marca: KE </li>" +
        "<li>SKU: 240717</li>" +
        "<li>Código de Barras: 7501244040638</li>" +
        "<li>Unidad: Pza.</li>" +
      "</ul>"
    "";
  }
  function cuaderno() {
    document.getElementById("Cuaderno").innerHTML = 
    "<ul>" +
        "<li>Marca: Norma </li>" +
        "<li>SKU: 430717</li>" +
        "<li>Código de Barras: 7541244040638</li>" +
        "<li>Unidad: Pza.</li>" +
      "</ul>"
    "";
  }
  function goma() {
    document.getElementById("Goma").innerHTML = 
      "<ul>" +
        "<li>Marca: Bic. </li>" +
        "<li>SKU: 3o0717</li>" +
        "<li>Código de Barras: 7544244040638</li>" +
        "<li>Unidad: Pza.</li>" +
      "</ul>"
    "";
  }
  function regla() {
    document.getElementById("Regla").innerHTML = 
      "<ul>" +
        "<li>Marca: Prisma </li>" +
        "<li>SKU: 244417</li>" +
        "<li>Código de Barras: 7501244123638</li>" +
        "<li>Unidad: Pza.</li>" +
      "</ul>"
    "";
  }
  function boligrafo() {
    document.getElementById("Boligrafo").innerHTML = 
      "<ul>" +
        "<li>Marca: Bic. </li>" +
        "<li>SKU: 249917</li>" +
        "<li>Código de Barras: 750124763638</li>" +
        "<li>Unidad: Pza.</li>" +
      "</ul>"
    "";
    
  }
  function sacapuntas() {
    document.getElementById("Sacapuntas").innerHTML = 
      "<ul>" +
        "<li>Marca: Tec. </li>" +
        "<li>SKU: 111717</li>" +
        "<li>Código de Barras: 7502224040638</li>" +
        "<li>Unidad: Pza.</li>" +
      "</ul>"
    "";
  }

 
  //_________________________________________________//
  /*
  * Funcion total
  * Calcula el total apartir del valor de cada articulo y su precio.
  * Se obtiene la cantidad introducido en la pagina con el metodo .value y se le asigna a una variable
  * Se obtiene el precio del articulo por lo que existe dentro de la etiqueta con un id de cada uno de los productos. 
  */
  //__________________________________________________//

 function total(){
    let amountLapiz = document.getElementById("AmountLapiz").value;
    let preciolapiz = document.getElementById("PrecioLapiz").innerHTML;


    let amountgoma = document.getElementById("AmountGoma").value;
    let preciogoma = document.getElementById("PrecioGoma").innerHTML;

    let amountcuaderno = document.getElementById("AmountCuaderno").value;
    let preciocuaderno = document.getElementById("PrecioCuaderno").innerHTML;

    let amountregla = document.getElementById("AmountRegla").value;
    let precioregla = document.getElementById("PrecioRegla").innerHTML;


    let amountboligrafo = document.getElementById("AmountBoligrafo").value;
    let precioboligrafo = document.getElementById("PrecioBoligrafo").innerHTML;

    let amountsacapuntas = document.getElementById("AmountSacapuntas").value;
    let preciosacapuntas = document.getElementById("PrecioSacapuntas").innerHTML;

    /**
     * Se hacen la asignacion de totales de los articulos a variables por separado 
     */

    let IVA = 0.16;
    let totalLapiz = preciolapiz * amountLapiz; 
    let totalCuaderno = preciocuaderno * amountcuaderno; 
    let totalgoma = preciogoma  * amountgoma;
    let totalregla = precioregla*amountregla; 
    let totalboligrafo = precioboligrafo * amountboligrafo; 
    let totalsacapuntas = preciosacapuntas * amountsacapuntas; 
    
    // La variable total hace la suma de los totales de todos los productos, se le añade el iva y se redondea el precio. 
    let total = Math.ceil(totalCuaderno + totalLapiz + totalboligrafo + totalgoma + totalregla + totalsacapuntas);
    let totalIva = total + (total * IVA);
  
    // Se añade de manera dinamica por medio de ".inner.HTML"
    document.getElementById("Total").innerHTML = "Total: $" + total + "<br> Total más iva: $" + totalIva;
   
  }

  /**
   * Funcion validar contraseña 
   * A traves de variables tipo bool se hace una secuencia if para validar todos los campos de la contraseña
   */
  function validarContraseña() {

    // Se obtienen los valores de la contraseña con el metodo value y se le asignan a una variable. 
    var contrasena = document.getElementById("Contrasena").value;
    var verificacion = document.getElementById("Verify").value;
    var length = false;
    var num = false;
    var matchL = false;
    var caracter = false;
    var pruebaAprobada = false;
    
    document.getElementById("Contrasena").value;
    
    //Valida tamaño 
    if(contrasena.length >= 8){
      length = true;
    }
    // Valida que contenga un numero 
    if(contrasena.match(/[0-9]/)){
      num = true;
    }
    // VAluda que tenga letras
    if(contrasena.match(/[a-z]/)){
      matchL = true;
    }
    // VAlida el caracter especial 
    if(contrasena.match(/[!#$%*&.-]/)){
      caracter = true;
    }
    // Si se cumplen todos los requerimentos se regresa true. 
    if(length == true && num == true && matchL == true && caracter == true){
      pruebaAprobada = true;
    }
    
    if(contrasena == verificacion && pruebaAprobada == false) {
        document.getElementById("AyudaPassword").innerHTML = "La contraseña no cumple los requisitos";
        document.getElementById("AyudaVerify").innerHTML = "La contraseña no cumple los requisitos";
        return false;
        
    } else if(contrasena == verificacion && pruebaAprobada == true) {
        document.getElementById("AyudaPassword").innerHTML = "Contraseña aceptada";
        document.getElementById("AyudaVerify").innerHTML = "Contraseña aceptada";
        return true;
        
    } else {
        document.getElementById("AyudaPassword").innerHTML = "Las contraseñas no coinciden";
        document.getElementById("AyudaVerify").innerHTML = "Las contraseñas no coinciden";
        return false;
    }
}
