/**
 * Funcion changeFont(): 
 * Cambia el tipo de letra en base a una variable de true o false. 
 * 
 */
var estado = 0;
function changeFont() {
	if(estado == 0) {
		document.getElementById("Fuente").style.fontFamily = "Perpetua";
		estado = 1;
	} else if(estado == 1) {
		document.getElementById("Fuente").style.fontFamily = "Brush Script MT";
		estado = 2;
	}else if(estado == 2) {
		document.getElementById("Fuente").style.fontFamily = "Monaco";
		estado = 0;
	}
}
/**
 * Funcion allowDrop: Permite mover una imagen a otro Div
 * Funcion drag: Permite jalar imagen o texto
 * Funcion drop: Permite soltar imagen en el div
 * @param {*} ev : Objeto imagen
 */

function allowDrop(ev) {
	ev.preventDefault();
  }
  
  function drag(ev) {
	ev.dataTransfer.setData("text", ev.target.id);
  }
  
  function drop(ev) {
	ev.preventDefault();
	var data = ev.dataTransfer.getData("text");
	ev.target.appendChild(document.getElementById(data));
  }

  //--------------------

  /**
   * Funcion avisoContraseña
   * Genera un alert al usuario para que no olvide las reglas
   */
  function avisoContraseña() {
	let avisos = 0;
	  if(avisos == 0) {
		  alert("Recuerda incluir mayusculas y caracteres especiales ");
	  }
	  avisos++;
  }
  
  /**
   * Funcion avisoVerify
   * Genera un alert al usuario para que sea la misma contraseña
   */
  
  function avisoVerify() {
	let avisos = 0;
	  if(avisos == 0) {
		  alert("Recuerda escribir nuevamente la contraseña");
	  }
	  avisos++;
  }
  
  /**
   * Funcion setInterval
   * Genera un contador del tiempo desde que se abre la pagina.  
   */
  var contador = 0;
setInterval(function(){ 
	contador++;
	document.getElementById("Contador").innerText = "Llevas: " + contador + " s. en la página.";
	if (contador >= 60){
		document.getElementById("Contador").innerHTML = "Llevas mas de 1 minuto apurate jeje.";
	}
}, 1000);