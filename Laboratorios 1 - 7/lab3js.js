/*
* Funcion potencia
* Genera por el numero ingresado el cuadrado y el cubo de 1 a n
*/
function potencia() {
    
    // Se pide al usuario que ingrese el numero a para generar el numero de cuadrados y  cubos
    var num = prompt("Escribe un numero para generar tabla ");
    // Genera la tabla con la funcion write() para HTML 
	document.write("<table border='1'");
	document.write("<thead>");
	document.write("<th>Numero</th> <th>Cuadrado</th> <th>Cubo</th>");
	document.write("</thead>");

    // Ciclo for que termina hasta el numero ingresado por el usuario
	for(var i = 1; i <= num; i++) {
        document.write("<tr>");
        // por medio de la  funcion math obtiene el numero al cubo o cuadrado  y lo  impime en  la tabla
		document.write("<td>"+i+"</td> <td>"+Math.pow(i,2)+"</td> <td>"+Math.pow(i,3)+"</td>");
		document.write("</tr>");
	}
	document.write("</table>");
}

//-----------------------------------------------------------------------------------------------------
/* 
* Funcion sumaRandom
* Hace la suma de dos numeros aleatorios e indica el tiempo en que fue resuelto 
*/
function sumaRandom() {
    //Genera el primer numero aleatorio
    var numeroUno = Math.floor(Math.random()*100);
    //Genera el segundo numero aleatorio 
    var numeroDos = Math.floor(Math.random()*100);
    //hace la suma de los dos numero aleatorios
	var suma = numeroUno + numeroDos;
    
    // El método performance.now() devuelve un DOMHighResTimeStamp, 
    // medido en milisegundos, con una precisión de cinco milésimas de segundo (5 microsegundos).
    var tiempo_inicio = performance.now();
    // Escribe la operacion para el usuario 
    var operacion = prompt("" + numeroUno + " + " + numeroDos + " = ");
    //Se saca  el tiempo final de la operacion
	var tiempo_final = performance.now();
	var tiempo_entre = ((tiempo_final - tiempo_inicio)/1000).toFixed(2);

    //Compara que la suma hecha por el usuario sea  correcta o incorrecta
	if(operacion == suma) {
		document.write("¡Correcto!" + "<br>");
	} else {
		document.write("¡Incorrecto!" + "<br>");
	}
	document.write("Tardaste " + tiempo_entre + " segundos en responder." + "<br>");
}

//----------------------------------------------------------------------------------

/*
* Funcion pideArray
* pide al usuario  que genere una lista de numeros
*/
function pideArray(){

    var arr = [];
    var num = prompt("Escribe cuantos numeros quieres en tu lista ");
    for (let i =0;i < num; i++){
        arr[i]=prompt("Escribe el numero (Positivo Negativo o Cero): ")
    }
    return arr;
}

/*
* Funcion contador
* apartir de la lista genrada por el usuario calcula el numero de positivos negativos o ceros
*/ 
function contador() {
    
    var arr = []
    arr = pideArray(); 
	
	var positivos = 0;
	var negativos = 0;
	var ceros = 0;
	for(i = 0; i < arr.length; i++) {
		if(arr[i] > 0) {
			positivos++;
		} else if (arr[i] == 0) {
			ceros++;
		} else if (arr[i] < 0) {
			negativos++;
		}
    }
    document.write("Tu lista de numeros es:  "+ arr);
	document.write("<br> Positivos = " + positivos + "<br>Ceros = " + ceros + "<br>Negativos = " + negativos);
}

//------------------------------------------------------

/*
* Funcion promedio
* Haze la suma de una matriz           
*/ 
function promedio() {
	var matriz = [];
	var suma = 0;
	var filas = 9;
	var columnas = 9;
	for(var i=0; i<filas; i++) {
	    matriz[i] = [];
	    for(var j=0; j<columnas; j++) {
	        matriz[i][j] = Math.floor(Math.random()*10);
	        document.write(matriz[i][j] + ", ")
	        suma = suma + matriz[i][j];
	    }
	    var promedio = suma/columnas;
		document.write("<br>Promedio = " + promedio.toFixed(2) + "<br><br>");
		suma = 0;
	}
}

//----------------------------------------------------

/*
* Funcion invertir numero 
* Parámetros: Un número. Regresa: El número con sus dígitos en orden inverso.
*
*/

function invertirNumero() {
	var num = prompt("Número a invertir = ")
	num = num + "";
	document.write(num.split("").reverse().join(""));
}


//-----------------------------------------

/**
 * Problema: Dado un numero, genera su equivalente en binario  e  indica si es o no un numero primo. 
 *  
 */

class Rectangulo {

	constructor(altura,base){
        this.altura = altura;
        this.base = base; 
	}

	areaRectanculo(){
        
        let area = (this.altura * this.base);
        document.write("<br>El area es: ", + area); 
	}
	perimetroRectangulo(){

        let perimetro = (2 * (this.base) + 2 * (this.altura) )
        document.write("<b>El perimetro es: ", + perimetro); 
        
    }
}
function analisisRectangulo(){
    var  altura = prompt("Escribe la altura del rectangulo: "); 
    var base = prompt("Escribe la base del rectangulo");
    var num = new Rectangulo(altura, base); 


	num.areaRectanculo();
	num.perimetroRectangulo();
	

	document.write('<br><br><a href="Laboratorio4.html">Laboratorio #4</a>');
}


