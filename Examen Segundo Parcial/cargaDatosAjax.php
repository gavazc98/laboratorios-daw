<?php
   require("conexion.php");


/**
 * Cargar datos implementando JSON y AJAX
 */

  
  header('Content-type: application/json; charset=utf-8');

  
  $conexion_bd = openDB();
  
    $conexion_bd->set_charset("utf8");
    $statement = $conexion_bd->prepare("SELECT I.nombre_incidente, L.nombre_lugar, IL.fecha
    FROM incidente as I, lugar as L, lugar_incidente as IL
    WHERE I.id_incidente = IL.id_incidente AND L.id_lugar = IL.id_lugar
    ORDER BY I.nombre_incidente");

    $statement->execute();
    $resultados = $statement->get_result();
    
    $respuesta = [];
    
    while($row = $resultados->fetch_assoc()){
      $usuario = [
        'nombre_incidente' 	=> $row['nombre_incidente'],
        'nombre_lugar' 	  	=> $row['nombre_lugar'],
        'fecha'		          => $row['fecha'],
      ];
      array_push($respuesta, $usuario);
    }

    echo json_encode($respuesta);
  

  ?>