
var btn_cargar = document.getElementById('btn_cargar_usuarios'),
error_box = document.getElementById('error_box'),
tabla = document.getElementById('tabla'),
loader = document.getElementById('loader');

function cargarUsuarios(){
tabla.innerHTML = '<tr><th>Nombre Incidente </th><th>Nombre Lugar </th><th>Fecha</th>';

var peticion = new XMLHttpRequest();
peticion.open('GET', 'cargaDatosAjax.php');

loader.classList.add('active');

peticion.onload = function(){
    var datos = JSON.parse(peticion.responseText);
    
        for(var i = 0; i < datos.length; i++){
            var elemento = document.createElement('tr');
            elemento.innerHTML += ("<td>" + datos[i].nombre_incidente + "</td>");
            elemento.innerHTML += ("<td>" + datos[i].nombre_lugar + "</td>");
            elemento.innerHTML += ("<td>" + datos[i].fecha + "</td>");
            tabla.appendChild(elemento);
        }
    
}

peticion.onreadystatechange = function(){
    if(peticion.readyState == 4 && peticion.status == 200){
        loader.classList.remove('active');
    }
}

peticion.send();
}

// $( document ).ready(function() {
//     cargarUsuarios();
// });
btn_cargar.addEventListener('click', function(){
cargarUsuarios();


});