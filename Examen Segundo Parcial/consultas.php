<?php

require("conexion.php");
/**
 * Funcicion para la carga de datos a la bd
 * 
 */
function registrar($nombre_incidente, $nombre_lugar) {
    
    $conexion_bd = openDB();  
    $query='CALL nuevoIncidente(?,?)';
  
    if (!($statement = $conexion_bd->prepare($query))) {
        die("No se pudo preparar la consulta para la base de datos: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
    }
      // Una S por placeholder que tengamos.
          // s = string
          // i = integer
          // d = double
    if (!$statement->bind_param("ii", $nombre_incidente, $nombre_lugar)) {
        die("Falló en la vinculación de los parámetros: (" . $statement->errno . ") " . $statement->error); 
    }
    if (!$statement->execute()) {
        die("Falló la ejecución de la consulta: (" . $statement->errno . ") " . $statement->error);
    } 
  
    closeDB($conexion_bd);
  }

  /**
   * Carga de datos sin uso de ajax
   */

  function consulta(){

    $conexion_bd = openDB();  
    $consulta_sql = "SELECT I.nombre_incidente, L.nombre_lugar, IL.fecha
    FROM incidente as I, lugar as L, lugar_incidente as IL
    WHERE I.id_incidente = IL.id_incidente AND L.id_lugar = IL.id_lugar
    ORDER BY I.nombre_incidente";


    $imprimir = '<table class="highlight">
    <thead>
        <tr>
            <th>Nombre incidente </th>
            <th>Nombre Lugar</th>
            <br>
            <th>Fecha</th>
        </tr>
    </thead>';
    $resultado = $conexion_bd->query($consulta_sql);

    while ($row = mysqli_fetch_array($resultado, MYSQLI_BOTH)) {
      
      $imprimir .= "<tr>";
      $imprimir .= "<td>".$row['nombre_incidente']."</td>"
                  ."<td>".$row['nombre_lugar']."</td>"
                  ."<td>".$row['fecha']."</td>";
        
      $imprimir .= "</tr>";
      }
    
    return $imprimir;
    closeDB($conexion_bd); 
  }
    
  ?>





?>