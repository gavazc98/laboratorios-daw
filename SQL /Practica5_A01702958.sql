CREATE DATABASE BasesDeDatos;

USE BasesDeDatos; 

CREATE TABLE Pelicula (
  titulo varchar(255) DEFAULT NULL,
  anio int(11) DEFAULT NULL,
  duracion int(11) DEFAULT NULL,
  encolor tinyint(1) DEFAULT NULL,
  presupuesto decimal(10, 0) DEFAULT NULL,
  nomestudio varchar(255) DEFAULT NULL,
  idproductor int(11) DEFAULT NULL
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

CREATE TABLE Elenco (
  titulo varchar(255) DEFAULT NULL,
  anio int(11) DEFAULT NULL,
  nombre varchar(255) DEFAULT NULL,
  sueldo decimal(10, 0) DEFAULT NULL
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

CREATE TABLE Actor (
  nombre varchar(255) DEFAULT NULL,
  direccion varchar(255) DEFAULT NULL,
  telefono int(11) DEFAULT NULL,
  fechanacimiento date DEFAULT NULL,
  sexo char(20) DEFAULT NULL
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

CREATE TABLE Productor (
  idProductor int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(255) DEFAULT NULL,
  direccion varchar(255) DEFAULT NULL,
  telefono int(11) DEFAULT NULL,
  PRIMARY KEY (idProductor)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

CREATE TABLE Estudio (
  nomestudio varchar(255) DEFAULT NULL,
  direccion varchar(255) DEFAULT NULL
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

/*
Consulta 1: 
El ingreso total recibido por cada actor, sin importar en cuantas películas haya participado.


SELECT E.nombre, SUM(sueldo) as 'Ingreso Total'
FROM Elenco as E, Pelicula as P
WHERE P.titulo = E.titulo 
GROUP BY E.nombre
*/

/*
Consulta 2: 
El monto total destinado a películas por cada Estudio Cinematográfico, durante la década de los 80's.
*/

SELECT Es.nomestudio, SUM(presupuesto) as 'Monto total'
FROM Estudio as Es, Pelicula as P 
WHERE Es.nomestudio = P.nomestudio 
AND P.anio BETWEEN '01/01/1981' AND '31/12/1990'
GROUP BY Es.nomestudio

/*
Consulta 3: 

Nombre y sueldo promedio de los actores (sólo hombres) que reciben en promedio un pago superior a 5 millones de dolares por película
*/

SELECT E.nombre, AVG(sueldo) as 'Sueldo Promedio' 
FROM Elenco as E, Actor as A
WHERE A.nombre = E.nombre AND A.sexo = 'masculino' 
HAVING ('Sueldo Promedio' > 5000000)
GROUP BY E.nombre 

/*
Consulta 4: 

Título y año de producción de las películas con menor presupuesto. (Por ejemplo, la película de Titanic se ha producido en varias veces entre la lista de películas estaría la producción de Titanic y el año que fue filmada con menor presupuesto).
*/

SELECT titulo, anio, MIN(presupuesto) as 'Presupuesto'
From Pelicula 
GROUP BY titulo 

/*
Consluta 5: 

Mostrar el sueldo de la actriz mejor pagada.

*/

SELECT nombre, MAX(sueldo) as 'Sueldo'
FROM Elenco
GROUP BY nombre 
