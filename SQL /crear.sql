
/*
Guillermo A. Vazquez Cervantes
Laboratorio 1 Bases de Datos. 
Crear.sql 
*/
#Verifica si existe una base de datos con el mismo nombre, si si la elimina y crea una nueva. 
DROP DATABASE IF EXISTS Laboratorio1DAW; 
#Crea una base de datos. 
CREATE DATABASE Laboratorio1DAW; 

USE Laboratorio1DAW; 
#Verifica que no exita una tabla con el mismo nombre, si si la elimina y crea una. 
DROP TABLE IF EXISTS Materiales; 
CREATE TABLE Materiales
(
  Clave numeric(5),
  Descripcion varchar(50),
  Costo numeric(8,2)
);
#Verifica que no exita una tabla con el mismo nombre, si si la elimina y crea una. 
DROP TABLE IF EXISTS Proveedores; 
CREATE TABLE Proveedores
(
	RFC char(13),
	RazonSocial varchar(50)
);
#Verifica que no exita una tabla con el mismo nombre, si si la elimina y crea una. 
DROP TABLE IF EXISTS Proyectos; 
CREATE TABLE Proyectos
(
	Numero numeric(5),
	Denominacion varchar(50)
);
#Verifica que no exita una tabla con el mismo nombre, si si la elimina y crea una. 
DROP TABLE IF EXISTS Entregan; 
CREATE TABLE Entregan
(
	Clave numeric(5),
	RFC char(13),
	Numero numeric(5),
	Fecha datetime,
	Cantidad Numeric(8,2)
);


/*
NO permite ejecutar el comadno sp_help, sql de XAMPP no reconoce ese comando
Help regresa algo similar sin embargo no es lo mismo. 
*/

HELP Materiales; 
HELP Entregan; 
HELP Proveedores; 
HELP Proyectos; 

#Elimina las tablas creadas 
DROP TABLE Materiales;
DROP TABLE Proveedores;
DROP TABLE Proyectos;
DROP TABLE Entregan;

#Verificar que ya no exitan las tablas 
select * from Materiales where xtype='U'; 
select * from Proveedores where xtype='U';
select * from Proyectos where xtype='U';
select * from Entregan where xtype='U';