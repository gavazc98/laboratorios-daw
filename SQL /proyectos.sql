
/*
Guillermo A. Vazquez Cervantes
Laboratorio 1 Bases de Datos. 
proyectos.sql 
*/

BULK INSERT Laboratorio1DAW.[Proyectos]
   FROM 'nfs://192.168.64.2/opt/lampp/htdocs/SQL/proyectos.csv'
   WITH 
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      );

SELECT  * FROM Proyectos;