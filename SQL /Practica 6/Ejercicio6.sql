

/*
* Guillermo Vazquez Cervnates 
* Practica 6

*/
-- 1)	Actrices de “Las brujas de Salem”

-- Sin  SubConsulta 
SELECT A.nombre
FROM Actor A, Elenco E
WHERE A.nombre = E.nombre AND E.título = “Las brujas de Salem” AND A.sexo = ‘F’


-- Con sub consulta
(SELECT nombre
FROM Elenco
WHERE título = “Las Brujas de Salem”)
MINUS
(SELECT nombre
FROM Actor 
WHERE sexo = ‘M’)



-- 2)	Nombres de los actores que aparecen en películas producidas por MGM en 1995. 

SELECT E.nombre 
FROM Elenco E, Película P
WHERE E.Título = P.título AND E.año = P.año AND P.nomestudio = ‘MGM’ AND P.año = 1995


SELECT nombre
FROM Elenco 
WHERE título IN
	(SELECT título
	FROM Película
	WHERE nomestudio = ‘MGM’ AND año = 1995)

-- 3)	Películas que duran más que “Lo que el viento se llevó” (de 1939)


SELECT P.título
FROM Película P, Película P2
WHERE P2.título = ‘Lo que el viento se llevó’ AND P2.año = 1939 AND P.duración > P2.duración

SELECT título
FROM Película
WHERE duración > 
(SELECT duración 
FROM Película 
WHERE título = ‘Lo que él viento se llevó’ AND año = 1939)



-- 4)	Productores que han hecho más películas que George Lucas. 



-- No se puede realizar la consulta sin una subconsulta

SELECT nombre, count(P.idproductor)
FROM Película P, Productor PR
WHERE P.idproductor=PR.productor
GROUP BY nombre
HAVING count(P.idproductor) > (
	SELECT count(P.idproductor)
	FROM Película P, Productor PR
	WHERE P.idproductor=PR.idproductor AND PR.nombre=”George Lucas”
)


-- 5)	Nombres de los productores de las películas en las que ha aparecido Sharon Stone. 


SELECT DISTINCT P.nombre
FROM Productor P, Película PE, Elenco E
WHERE P.idproductor = PE.idproductor AND P.año = PE.año AND PE.titulo = E.titulo AND E.nombre='Sharon Stone'



SELECT DISTINCT P.nombre
FROM ProductorP
WHERE P.idproductor IN (
    SELECT PE.idproductor
    FROM Elenco E, Película PE
    WHERE E.titulo=PE.titulo AND E.nombre='Sharon Stone'
)



-- 6)	Título de las películas que han sido filmadas más de una vez


SELECT título
FROM Películas 
GROUP BY título
HAVING count(*) > 1



(SELECT título
FROM Películas 
GROUP BY título)
MINUS 
(SELECT título
FROM Películas 
GROUP BY título
HAVING count(*) = 1
)

