
/*
Guillermo A. Vazquez Cervantes
Laboratorio 1 Bases de Datos. 
entregan.sql 
*/
SET DATEFORMAT dmy
BULK INSERT Laboratorio1DAW.[Entregan]
   FROM 'nfs://192.168.64.2/opt/lampp/htdocs/SQL/entregan.csv'
   WITH 
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )

SELECT  * FROM Entregan;