<?php
// Creacion de librearias para acceso  a base de datos. 


/*
*  Funcion conectar_BD : Conecta a la base de datos
*/ 
function conectar_bd() {
    $servidor = "localhost"; 
    $User = "root"; 
    $Password = ""; 
    $DataBaseName = "name_DB";

    $conexion_bd = mysqli_connect($servidor,$User,$Password,$DataBaseName);
        if ($conexion_bd == NULL) {
            die("No se pudo conectar con la base de datos");
        }
    
        return $conexion_bd;
}

/**
 * Function close DB: Cierra la conexion con la base de datos. 
 * @param: $conexiondb: Variable con los datos de conexion. 
*/

function close_bd($conexion_bd){
    mysqli_close($conexion_bd);  
}

?>