/*
Guillermo A. Vazquez Cervantes
Laboratorio 13 Bases de Datos. 
CrearBd.sql 
*/

/*
Crearmos procedimiento para que se ejecute si la tabla existe, ya que en caso contrario mostrará error.
*/

#Verifica si existe una base de datos con el mismo nombre, si si la elimina y crea una nueva. 
DROP DATABASE IF EXISTS Laboratorio1DAW; 
#Crea una base de datos. 
CREATE DATABASE Laboratorio1DAW; 

USE Laboratorio1DAW; 


/*
* NOTA: En MYSQL la sentencia directa de IF Exist no es valida, es necesario crear un procedimiento como el siguiente,
* sin embargo tampoco logro encontrar informacion en mysql acerca de INFORMATION_SCHEMA ya que dice que es una base de datos virtual pero 
* me genera un error, espero que asi este bien sin embargo la sentencia DROP TABLE IF EXISTS Materiales logra cumplir el mismo objetivo de destruir 
* tablas anteriores y crear una nueva. 
*/
CREATE PROCEDURE validar_Tablas(IN table_Name  varchar(20))
BEGIN
IF EXISTS (SELECT * FROM Laboratorio1DAW.TABLES WHERE TABLE_NAME = table_Name);
END IF;
END

#Verifica que no exita una tabla con el mismo nombre, si si la elimina y crea una. 
#DROP TABLE IF EXISTS Materiales; 

CALL validar_Tablas(Materiales);
CREATE TABLE Materiales
(
  Clave int(5) NOT NULL,
  Descripcion varchar(50) DEFAULT NULL,
  Costo int(8,2) DEFAULT NULL,
  PRIMARY KEY (Clave)
);
#Verifica que no exita una tabla con el mismo nombre, si si la elimina y crea una. 
#DROP TABLE IF EXISTS Proveedores;
CALL validar_Tablas(Proveedores); 
CREATE TABLE Proveedores
(
	RFC char(13) NOT NULL AUTO_INCREMENT,
	RazonSocial varchar(50) DEFAULT NULL,
	PRIMARY KEY (RFC)
);
#Verifica que no exita una tabla con el mismo nombre, si si la elimina y crea una. 
#DROP TABLE IF EXISTS Proyectos; 
CALL validar_Tablas(Proyectos);
CREATE TABLE Proyectos
(
	Numero int(5),
	Denominacion varchar(50) DEFAULT NULL,
	PRIMARY KEY (Numero)
);
#Verifica que no exita una tabla con el mismo nombre, si si la elimina y crea una. 
#DROP TABLE IF EXISTS Entregan; 
CALL validar_Tablas(Entregan);
CREATE TABLE Entregan
(
	Clave int(5) NOT NULL,
	RFC char(13) NOT NULL,
	Numero int(5) NOT NULL,
	Fecha datetime NOT NULL,
	Cantidad int(8,2) DEFAULT NULL,
	PRIMARY KEY (Clave, RFC, Numero, Fecha)
);


/*
* Carga de datos cada una de las tablas.
*/

BULK INSERT Laboratorio1DAW.materiales
   FROM 'nfs://192.168.64.2/opt/lampp/htdocs/SQL/materiales.csv'
   WITH 
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      );

SELECT  * FROM Materiales;

BULK INSERT Laboratorio1DAW.Proveedores
   FROM 'nfs://192.168.64.2/opt/lampp/htdocs/SQL/proveedores.csv'
   WITH 
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      );

SELECT  * FROM Proveedores;

BULK INSERT Laboratorio1DAW.Proyectos
   FROM 'nfs://192.168.64.2/opt/lampp/htdocs/SQL/proyectos.csv'
   WITH 
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      );

SELECT  * FROM Proyectos;

SET DATEFORMAT dmy
BULK INSERT Laboratorio1DAW.Entregan
   FROM 'nfs://192.168.64.2/opt/lampp/htdocs/SQL/entregan.csv'
   WITH 
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )

SELECT  * FROM Entregan;


/*
FP-help no existe en mysql, show tables muestra las tablas creadas. 
*/
SHOW TABLES; 


#Ejercicio #2
INSERT INTO Materiales values(1000,'xxx',1000);
SELECT * FROM Materiales;
DELETE FROM Materiales WHERE Clave = 1000 and Costo = 1000; 

# ALTER TABLE Materiales add constraint llaveMateriales PRIMARY KEY (Clave) --> Asi seria para hacer el constraint sin embargo defini el Primary Key dentro de la tabla para evitar errores 
SHOW TABLES; 
INSERT INTO Materiales values(1000,'xxx',1000);

#ALTER TABLE Proveedores ADD CONSTRAINT llaveProveedores PRIMARY KEY (RFC); --> Asi seria para hacer el constraint sin embargo defini el Primary Key dentro de la tabla para evitar errores 
#ALTER TABLE Proyectos ADD CONSTRAINT llaveProyectos PRIMARY KEY (Numero); --> Asi seria para hacer el constraint sin embargo defini el Primary Key dentro de la tabla para evitar errores 
#ALTER TABLE Entregan ADD CONSTRAINT llaveEntregan PRIMARY KEY (Clave, RFC, Numero, Fecha); --> Asi seria para hacer el constraint sin embargo defini el Primary Key dentro de la tabla para evitar errores 

#Ejercicio #3
SELECT * FROM Materiales;
SELECT * FROM Proveedores;
SELECT * FROM Proyectos;

INSERT INTO Entregan values(0,'xxx',0,'1-jan-02',0);
SELECT * FROM Entregan;
DELETE FROM Entregan WHERE Clave = 0;

/*
* Se generan las llaves foraneas 
*
*/
ALTER TABLE Entregan ADD CONSTRAINT cfentreganclave FOREIGN KEY (clave) REFERENCES materiales(clave);
INSERT INTO Entregan values(0,'xxx',0,'1-jan-02',0);
ALTER TABLE entregan ADD CONSTRAINT cfentreganrfc FOREIGN KEY (RFC) REFERENCES proveedores(RFC);
ALTER TABLE entregan ADD CONSTRAINT cfentregannumero FOREIGN KEY (numero) REFERENCES proyectos(numero);

SHOW TABLES; 

# Ejercicio #4
INSERT INTO Entregan values(1000, 'AAAA800101', 5000, GETDATE(), 0);
SELECT * FROM Entregan; 
DELETE FROM Entregan WHERE Cantidad = 0; 
ALTER TABLE Entregan ADD CONSTRAINT cantidad CHECK (cantidad > 0); 
INSERT INTO entregan values(1000, 'AAAA800101', 5000, GETDATE(), 0);

SHOW TABLES; 




/* PREGUNTAS DE INVESTIGACION 
Laboratorio #13

1)	Revisa el contenido de la tabla materiales y determina si existe alguna inconsistencia en el contenido de la tabla al agregar el registro ¿Cuál es? ¿A qué se debe?
 El registro se inserta al final de la tabla porque no se ordena automáticamente
 Existen dos registros con clave igual a 1000
 No está establecida la llave primaria de la tabla Materiales, por lo que no se está aplicando la regla de integridad referencial asociada a las claves
2)	¿Qué ocurrió al ejecutar el INSERT INTO (1000,’xxx’,1000) con la llave primaria ya establecida en Materiales?
 No se ejecutó porque ya existe un registro con clave 1000
3)	¿Qué información muestra la consulta sp_helpconstraint materiales? 
 Las llaves primarias de la tabla, sin embargo con showtables muestras toda la info de las tablas ya que el comando sp_helpconstraint no exite en mysql
4)	¿Qué sentencias utilizaste para definir las llaves primarias de Proveedores y Proyectos? 
 ALTER TABLE Proveedores add constraint llaveProveedores PRIMARY KEY (RFC), sin embargo es recomendable nombrarlas al incio de la creacion de la tabla con la sentencia Primary Key (nombre de columna)
5)	¿Qué sentencias utilizaste para definir el constraint de Entregan? 
 ALTER TABLE Entregan add constraint llaveEntregan PRIMARY KEY (Clave, RFC, Numero, Fecha)
6)	¿Qué particularidad observas en los valores para clave, RFC y numero? 
¿Cómo responde el sistema a la inserción de este registro? 
 La inserta al inicio de la tabla de forma normal a pesar de que las claves inician en 1000, los RFCs tienen 13 caracteres y los Números empiezan en 5000 
7)	¿Qué significa el mensaje que emite el sistema al reinsertar el registro inconsistente? ¿Qué significado tiene la sentencia anterior?
 El registro tiene una llave primaria inconsistente porque viola la integridad referencial.
 No se puede poner un registro cuya clave sea menor a 1000.
8)	¿Qué significan las columnas de esas consultas?
 Son llaves foráneas que aseguran la unicidad e integridad de los registros de Entregan 
9)	¿Qué uso se le está dando a GETDATE()?
¿Tiene sentido el valor del campo de cantidad? 
GETDATE() obtiene los datos del día de hoy 
No tiene sentido que una entrega haya sido de una cantidad nula
10)	Intenta insertar un registro con cantidad igual o menor que 0. 
¿Cómo responde el sistema? ¿Qué significa el mensaje? 
 No inserta el registro porque viola la regla de integridad referencial de que la cantidad no puede ser menor o igual a 0
11)	Investiga el concepto de integridad referencial
Estrictamente hablando, para que un campo sea una clave foránea, éste necesita ser definido como tal al momento de crear una tabla. Se pueden definir claves foráneas en cualquier tipo de tabla de MySQL, pero únicamente tienen sentido cuando se usan tablas del tipo InnoDB.

A partir de la versión 3.23.43b, se pueden definir restricciones de claves foráneas con el uso de tablas InnoDB. InnoDB es el primer tipo de tabla que permite definir estas restricciones para garantizar la integridad de los datos.






*/