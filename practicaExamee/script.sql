CREATE TABLE `lugar` (
  `id_lugar` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_lugar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_lugar`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;


CREATE TABLE `incidente` (
  `id_incidente` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_incidente` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_incidente`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

#Relacion n a n, generacion de una nueva tabla. 

CREATE TABLE `lugar_incidente` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_lugar` int(11) DEFAULT NULL,
  `id_incidente` int(11) DEFAULT NULL,
  `fecha` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;



# Generacion de store procediur

CREATE DEFINER=`dawbdorg_1702958`@`%.%.%.%` PROCEDURE `nuevoIncidente`(
IN uNombre int(50),
IN uNombre_incidente int(50)
)
BEGIN
    
    INSERT INTO lugar_incidente(id_lugar, id_incidente) VALUES (uNombre, uNombre_incidente);
END