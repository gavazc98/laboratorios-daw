<?php
   require("conexion.php");


/**
 * Cargar datos implementando JSON y AJAX
 */

  
  header('Content-type: application/json; charset=utf-8');

  
  $conexion_bd = openDB();
  
    $conexion_bd->set_charset("utf8");
    $statement = $conexion_bd->prepare("SELECT Z.nombre, E.estado, ZE.fecha
    FROM zombie as Z, estado as E, zombie_estado as ZE
    WHERE Z.id_zombie = ZE.id_zombie AND E.id_estado = ZE.id_estado
    ORDER BY Z.nombre");

    $statement->execute();
    $resultados = $statement->get_result();
    
    $respuesta = [];
    
    while($row = $resultados->fetch_assoc()){
      $usuario = [
        'nombre' 		=> $row['nombre'],
        'estado' 	=> $row['estado'],
        'fecha'		=> $row['fecha'],
      ];
      array_push($respuesta, $usuario);
    }

    echo json_encode($respuesta);
  

  ?>