<?php
   require("conexion.php");


/**
 * Cargar datos implementando JSON y AJAX
 * 
 * 
 */
function consulta_zombie_ajaxx(){
  
  header('Content-type: application/json; charset=utf-8');
  $conexion_bd = openDB();
  
    $conexion_bd->set_charset("utf8");
    $statement = $conexion_bd->prepare("SELECT Z.nombre, E.estado, ZE.fecha
    FROM zombie as Z, estado as E, zombie_estado as ZE
    WHERE Z.id_zombie = ZE.id_zombie AND E.id_estado = ZE.id_estado
    ORDER BY Z.nombre");

    $statement->execute();
    $resultados = $statement->get_result();
    
    $respuesta = [];
    
    while($row = $resultados->fetch_assoc()){
      $usuario = [
        'nombre' 		=> $row['nombre'],
        'estado' 	=> $row['estado'],
        'fecha'		=> $row['fecha'],
      ];
      array_push($respuesta, $usuario);
    }

    return json_encode($respuesta);
  }




  function consulta_zombie(){

    $conexion_bd = openDB();  
    $consulta_sql = "SELECT Z.nombre, E.estado, ZE.fecha
    FROM zombie as Z, estado as E, zombie_estado as ZE
    WHERE Z.id_zombie = ZE.id_zombie AND E.id_estado = ZE.id_estado
    ORDER BY Z.nombre"; 


    $imprimir = '<table class="highlight">
    <thead>
        <tr>
            <th>Zombie</th>
            <th>Estado</th>
            <br>
            <th>Fecha</th>
        </tr>
    </thead>';
    $resultado = $conexion_bd->query($consulta_sql);

    while ($row = mysqli_fetch_array($resultado, MYSQLI_BOTH)) {
      
      $imprimir .= "<tr>";
      $imprimir .= "<td>".$row['nombre']."</td>"
                  ."<td>".$row['estado']."</td>"
                  ."<td>".$row['fecha']."</td>";
        
      $imprimir .= "</tr>";
      }
    
    return $imprimir;
    closeDB($conexion_bd); 
  }

// insertar nuevos zombies 

function registrar_zombie($nombre, $estado) {
    
  $conexion_bd = openDB();  
  $query='CALL nuevoZombie(?,?)';

  if (!($statement = $conexion_bd->prepare($query))) {
      die("No se pudo preparar la consulta para la base de datos: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
  }
    // Una S por placeholder que tengamos.
		// s = string
		// i = integer
		// d = double
  if (!$statement->bind_param("ss", $nombre, $estado)) {
      die("Falló en la vinculación de los parámetros: (" . $statement->errno . ") " . $statement->error); 
  }
  if (!$statement->execute()) {
      die("Falló la ejecución de la consulta: (" . $statement->errno . ") " . $statement->error);
  } 

  closeDB($conexion_bd);
}
  
?>