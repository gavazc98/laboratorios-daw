
var btn_cargar = document.getElementById('btn_cargar_usuarios'),
	error_box = document.getElementById('error_box'),
	tabla = document.getElementById('tabla'),
	loader = document.getElementById('loader');

// var usuario_nombre,
// 	usuario_edad,
// 	usuario_pais,
// 	usuario_correo;

function cargarUsuarios(){
	tabla.innerHTML = '<tr><th>Zombie</th><th>Estado</th><th>Fecha</th>';

	var peticion = new XMLHttpRequest();
	peticion.open('GET', 'cargaDatosAjax.php');

	loader.classList.add('active');

	peticion.onload = function(){
        var datos = JSON.parse(peticion.responseText);
        
			for(var i = 0; i < datos.length; i++){
				var elemento = document.createElement('tr');
				elemento.innerHTML += ("<td>" + datos[i].nombre + "</td>");
				elemento.innerHTML += ("<td>" + datos[i].estado + "</td>");
				elemento.innerHTML += ("<td>" + datos[i].fecha + "</td>");
				tabla.appendChild(elemento);
			}
		
		
	}

	peticion.onreadystatechange = function(){
		if(peticion.readyState == 4 && peticion.status == 200){
			loader.classList.remove('active');
		}
	}

	peticion.send();
}


btn_cargar.addEventListener('click', function(){
	cargarUsuarios();
});